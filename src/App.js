import React, { Component } from 'react';
import videojs from 'video.js'
import './App.css';
var youbora = require('youboralib');
require('youbora-adapter-videojs');
window.plugin = new youbora.Plugin({ accountCode: 'rikstvdev' });

class App extends Component {
  componentDidMount() {
    const config = {
      autoplay: true,
      controls: true,
    };
    this.player = videojs(this.videoNode, config, () => {
      this.player.src('http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4');
    });

    window.player = this.player;
    window.plugin.setAdapter(new youbora.adapters.Videojs(this.videoNode));
  }

  componentWillUnmount() {
    if (this.player) {
      this.player.dispose()
    }
  }

  render() {
    return (
      <div data-vjs-player>
        <video ref={ node => this.videoNode = node } className="video-js"></video>
      </div>
    );
  }
}

export default App;
